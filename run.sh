kubectl apply -f dbpv.yml 
kubectl apply -f dbpvc.yml -n wordpress2
kubectl apply -f mariadb-deployment.yml -n wordpress2
kubectl apply -f mariadb-service.yml -n wordpress2
kubectl apply -f wppv.yml 
kubectl apply -f wppvc.yml -n wordpress2
kubectl apply -f wp-deployment.yml -n wordpress2
kubectl apply -f nodeport.yml -n wordpress2

kubectl delete -f dbpvc.yml -n wordpress2
kubectl delete -f dbpv.yml
kubectl delete -f mariadb-deployment.yml -n wordpress2
kubectl delete -f mariadb-service.yml -n wordpress2
kubectl delete -f wppvc.yml -n wordpress2
kubectl delete -f wppv.yml
kubectl delete -f wp-deployment.yml -n wordpress2
kubectl delete -f nodeport.yml -n wordpress2