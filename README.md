# Wordpress

This README file details the way to set up wordpress from scratch and also how to re establish it should the stystem go down.

## Prerequisits 

- A K8 cluster with minimum (larger is recomended as this is still close to the minimum requirements of kubernetes but larger machines come with larger associated coset):
  - 1 k8 control panel - t3a.xlarge (30gb)
  - 2 k8 worker nodes - t3a.xlarge (20gb)
- Clone this repo on the K8 controller `git clone git@bitbucket.org:garthj98/wordpress-p4.git`

### Initial set up

```bash 
## Enter this command (it will be needed every time you log in to kubernetes to do anything)
$ sudo cp /etc/kubernetes/admin.conf $HOME/ && sudo chown $(id -u):$(id -g) $HOME/admin.conf && export KUBECONFIG=$HOME/admin.conf

## Create a namespace called 'wordpress'
$ kubectl create ns wordpress

## Change directory in to wordpress
$ cd wordpress

## Create the persistent volumes for wordpress and the mariadb
$ kubectl apply -f volumes/dbpv.yml 
$ kubectl apply -f volumes/wppv.yml 

## Create teh peristent volume claim for wordpress and mariabd 
$ kubectl apply -f volumes/dbpvc.yml -n wordpress
$ kubectl apply -f volumes/wppvc.yml -n wordpress

## Create the mariadb deployment and service 
$ kubectl apply -f wordpress/mariadb-deployment.yml -n wordpress
$ kubectl apply -f wordpress/mariadb-service.yml -n wordpress

## Create the wordpress pod and expose the nodeport 
$ kubectl apply -f wordpress/wp-deployment.yml -n wordpress
$ kubectl apply -f wordpress/nodeport.yml -n wordpress
```

If the system has crashed or not gone completely down there are a few things to do. (**please note if deleteing the nodeport script is ever involved and you are using a loadbalancer this loadbalancer may need to be reconfigured**)

1. Delete the pods for mariaDB and wordpress and re apply them (Due to teh persistent volumes all data will still be present)
```bash 
## Delete existing services and pods
$ kubectl delete -f wordpress/mariadb-deployment.yml -n wordpress
$ kubectl delete -f wordpress/mariadb-service.yml -n wordpress
$ kubectl delete -f wordpress/wp-deployment.yml -n wordpress
$ kubectl delete -f wordpress/nodeport.yml -n wordpress

## Create services and pods
$ kubectl apply -f wordpress/mariadb-deployment.yml -n wordpress
$ kubectl apply -f wordpress/mariadb-service.yml -n wordpress
$ kubectl apply -f wordpress/wp-deployment.yml -n wordpress
$ kubectl apply -f wordpress/nodeport.yml -n wordpress
```

2. Reboot the kubernetes cluster
  

Fernando
cohort9


